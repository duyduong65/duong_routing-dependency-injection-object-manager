<?php

namespace Information\Internship\Model\ResourceModel\Internship;

use Information\Internship\Model\Internship;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'information_internship_internship';
    protected $_eventObject = 'internship_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Internship::class,
            \Information\Internship\Model\ResourceModel\Internship::class
        );
    }
}
