<?php

namespace Information\Internship\Model;

class Internship extends \Magento\Framework\Model\AbstractModel implements
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'information_internship_internship';

    protected $_cacheTag = 'information_internship_internship';

    protected $_eventPrefix = 'information_internship_internship';

    protected function _construct()
    {
        $this->_init('Information\Internship\Model\ResourceModel\Internship');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
