<?php

namespace Information\Internship\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Save extends Action
{
    protected $_internship;
    public function __construct(
        Context $context,
        \Information\Internship\Model\InternshipFactory $internshipFactory)
    {
        parent::__construct($context);
        $this->_internship = $internshipFactory;
    }

    public function execute()
    {
        $data = $this->_request->getParams();
        $this->_internship->create()->setData($data)->save();
    }
}
