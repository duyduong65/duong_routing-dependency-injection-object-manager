<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Add\Information\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Custom Class
 *
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $data = [
            [
                'name' => 'Le Duy Duong',
                'avatar' => 'http://localhost/m0001/pub/media/image1.png',
                'dob' => '1997/06/06',
                'description' => 'vui tinh'
            ],
            [
                'name' => 'An Xuan Bach',
                'avatar' => 'http://localhost/m0001/pub/media/image2.png',
                'dob' => '1992/04/04',
                'description' => 'nhiet tinh'
            ],[
                'name' => 'Le Thanh Hai',
                'avatar' => 'http://localhost/m0001/pub/media/image3.png',
                'dob' => '1992/12/04',
                'description' => 'dep trai'
            ],[
                'name' => 'Tran Le Binh An',
                'avatar' => 'http://localhost/m0001/pub/media/image4.png',
                'dob' => '2000/08/20',
                'description' => 'xinh gai, dang yeu'
            ],[
                'name' => 'Le Ngoc Bao',
                'avatar' => 'http://localhost/m0001/pub/media/image5.png',
                'dob' => '2001/11/04',
                'description' => 'hai huoc'
            ],
        ];

        foreach ($data as $bind) {
                $setup->getConnection()
                    ->insertForce($setup->getTable('internship'), $bind);
        }
    }
}
