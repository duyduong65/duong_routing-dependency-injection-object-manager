<?php

namespace Add\Information\Model\ResourceModel\Users;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Custom method
     */
    protected function _construct()
    {
        $this->_init(
            'Add\Information\Model\Users',
            'Add\Information\Model\ResourceModel\Users'
        );
    }
}
