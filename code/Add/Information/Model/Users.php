<?php
namespace Add\Information\Model;

class Users extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init('Add\Information\Model\ResourceModel\Users');
    }
}
