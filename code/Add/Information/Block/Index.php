<?php
namespace Add\Information\Block;

use Magento\Framework\View\Element\Template;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_userFactory;
    public function __construct(
        Template\Context $context,
        array $data = [],
        \Add\Information\Model\UsersFactory $usersFactory
    ) {
        $this->_userFactory = $usersFactory;
        parent::__construct($context, $data);
    }
    public function getId()
    {
        $id = $this->getRequest()->getParams('id');
        $userFactory = $this->_userFactory->create()->load($id);
        return $userFactory->getData();
    }
}
